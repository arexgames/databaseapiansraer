package net.arexgames.database;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.mysql.main.SQLTools;

public class JoinEvent implements Listener{
	
	
	@EventHandler
	public void JoinEvent(PlayerJoinEvent e){
		
		
		String uuid = e.getPlayer().getUniqueId().toString();
		String pname = e.getPlayer().getName();
		
		
		SQLTools.setString("PlayerData", "UUID", "P_NAME", uuid, pname);
		for(String table : DataBaseAPIAnsraer.tableAutoAddPlayers){
			SQLTools.createValue(table, "UUID", uuid);
		}
		
	}
	
	
	public static void addPlayerToDatabase(String table, String datenabfrage, String uuid, String values) {
		//Creates first entry into database if player is not in database yet.
		if(!(SQLTools.valueExists(table, datenabfrage, uuid))){
				Bukkit.getServer().broadcastMessage("You are now beeing added to the database");
				DataBaseAPIAnsraer.mysql.update("INSERT INTO "+table+" VALUES ("+values+");");
		}
		
	}
}
