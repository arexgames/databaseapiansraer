package net.arexgames.database;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.mysql.main.MySQL;


public class DataBaseAPIAnsraer extends JavaPlugin{
	
	public static MySQL mysql;
	public static List<String> tableAutoAddPlayers = new ArrayList<String>();

	public void onEnable(){
		
		System.out.println("[MySQL] Plugin wurde gestartet");
		ConnectMySQL();
		PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(new JoinEvent(), this);
		
	}
	
	
	public void onDisable(){
		
		System.out.println("[MySQL] Plugin wurde gestoppt");
		
	}
	
	private void ConnectMySQL(){
		mysql = new MySQL("ni343346_1.vweb06.nitrado.net", "ni343346_1sql5", "ni343346_1sql5", "g5jyx1ayxc9123");
		mysql.update("CREATE TABLE IF NOT EXISTS PlayerData(UUID varchar(64), P_NAME varchar(64), RANK_ID int);");
		mysql.update("CREATE TABLE IF NOT EXISTS Ranks(RANK_ID int, RANK_NAME varchar(64), RANK_COLOR varchar(64), RANK_POWER int);");
	}

	/**
	 * Table that require the players UUID will have it added as soon as the player joins. 
	 * WARNING! Table must have an 'UUID' column!
	 * 
	 * @param tableName Name of the already created Table
	 */
	public void addAutoAddPlayersTable(String tableName){
		tableAutoAddPlayers.add(tableName);
	}
	
}
